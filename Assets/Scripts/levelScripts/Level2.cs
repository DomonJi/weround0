﻿using UnityEngine;
using System.Collections;

public class Level2 : Level
{
	ParticleSystem[] particles;

	protected override void Awake ()
	{
		base.Awake ();
		particles = GetComponentsInChildren <ParticleSystem> (true);
	}

	protected override void Start ()
	{
		base.Start ();
		for (int i = 0; i < particles.Length; i++) {
			particles [i].Simulate (8f);
		}
	}

	public override void Selected ()
	{
		base.Selected ();
		for (int i = 0; i < particles.Length; i++) {
			particles [i].Simulate (8f);
			particles [i].Play ();
		}
	}

	public override void UnSelected ()
	{
		base.UnSelected ();
		for (int i = 0; i < particles.Length; i++) {
			particles [i].Pause ();
		}
	}

	public override void OnMouseDown ()
	{
		base.OnMouseDown ();
		if (isSelected) {
			GetComponentInChildren<CrystalShining> ().enabled = true;
		}
	}

	public override void OnMouseExit ()
	{
		base.OnMouseExit ();
		GetComponentInChildren<CrystalShining> ().enabled = false;
	}

	public override void OnMouseUp ()
	{
		base.OnMouseUp ();
		GetComponentInChildren<CrystalShining> ().enabled = false;
	}
}
