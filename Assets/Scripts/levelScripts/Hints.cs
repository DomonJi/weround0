﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Hints : MonoBehaviour
{
	[SerializeField]Button cancle;

	void OnEnable ()
	{
		StartCoroutine (CanClick ());
	}

	IEnumerator CanClick ()
	{
		float timer = Time.realtimeSinceStartup;
		yield return new WaitUntil (() => Time.realtimeSinceStartup - timer > 1.5f);
		cancle.interactable = true;
	}

	public void HintBack ()
	{
		GetComponent<Animator> ().SetTrigger ("HintBack");
	}

	void OnDisable ()
	{
		cancle.interactable = false;
	}
}
