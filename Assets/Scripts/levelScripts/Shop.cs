﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Shop : SceneSingleton<Shop>
{
	public Text explodeNum;
	public Text antiNum;
	public Text ulrayNum;
	public GoldShow goldShow;
	[SerializeField]Animator fail1;
	[SerializeField]Animator fail2;
	[SerializeField]Animator fail3;
	MyAudio myAudio;

	void Awake ()
	{
		goldShow.preGold = PlayerPrefs.GetInt ("Gold");
		myAudio = GetComponent <MyAudio> ();
		//myAudio.audioSource = GetComponent <AudioSource> ();
	}

	void Update ()
	{
		explodeNum.text = PlayerPrefs.GetInt ("Explode").ToString ();
		antiNum.text = PlayerPrefs.GetInt ("Antibiotic").ToString ();
		ulrayNum.text = PlayerPrefs.GetInt ("UlRay").ToString ();
	}

	public void Back ()
	{
		SceneManager.LoadScene ("Level" + (GameController.Instance.currentLevel + 1));
	}

	public void BuyExplode ()
	{
		if (PlayerPrefs.GetInt ("Gold") >= 400) {
			goldShow.preGold = PlayerPrefs.GetInt ("Gold");
			PlayerPrefs.SetInt ("Gold", PlayerPrefs.GetInt ("Gold") - 400);
			PlayerPrefs.SetInt ("Explode", PlayerPrefs.GetInt ("Explode") + 1);
			myAudio.Play ();
		} else {
			fail1.SetTrigger ("Show");
		}
	}

	public void BuyAntibiotic ()
	{
		if (PlayerPrefs.GetInt ("Gold") >= 350) {
			goldShow.preGold = PlayerPrefs.GetInt ("Gold");
			PlayerPrefs.SetInt ("Gold", PlayerPrefs.GetInt ("Gold") - 350);
			PlayerPrefs.SetInt ("Antibiotic", PlayerPrefs.GetInt ("Antibiotic") + 1);
			myAudio.Play ();
		} else {
			fail3.SetTrigger ("Show");
		}
	}

	public void BuyUlray ()
	{
		if (PlayerPrefs.GetInt ("Gold") >= 300) {
			goldShow.preGold = PlayerPrefs.GetInt ("Gold");
			PlayerPrefs.SetInt ("Gold", PlayerPrefs.GetInt ("Gold") - 300);
			PlayerPrefs.SetInt ("UlRay", PlayerPrefs.GetInt ("UlRay") + 1);
			myAudio.Play ();
		} else {
			fail2.SetTrigger ("Show");
		}
	}
}
