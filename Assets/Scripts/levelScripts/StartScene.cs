﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartScene : MonoBehaviour
{
	[SerializeField]Button quitButton;

	public void Start ()
	{
		if (Application.platform == RuntimePlatform.Android) {
			quitButton.gameObject.SetActive (true);
		}
	}

	public void StartGame ()
	{
		
		SceneManager.LoadScene ("LevelsSelect");
	}

	public void About ()
	{
		SceneManager.LoadScene ("About");
	}

	public void QuitApp ()
	{
		Application.Quit ();
	}
}
