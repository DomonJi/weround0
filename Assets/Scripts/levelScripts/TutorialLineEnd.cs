﻿using UnityEngine;
using System.Collections;

public class TutorialLineEnd : MonoBehaviour
{
	public Animator tutorialAnimator;

	public void LineEnd ()
	{
		tutorialAnimator.SetTrigger ("Bomb");
	}

	public void CanNext ()
	{
		Dialog.Instance.OK.gameObject.SetActive (true);
	}
}
