﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Level : MonoBehaviour
{

	public Transform halo;
	public int levelIndex;
	public bool isSelected = true;
	public Animator[] animators;
	public MyAudio[] myaudios;
	[SerializeField]MyAudio parentAudio;

	SpriteRenderer[] sprites;

	public Material greyMat;

	protected virtual void Awake ()
	{
		sprites = GetComponentsInChildren <SpriteRenderer> ();
		animators = GetComponentsInChildren<Animator> ();
		myaudios = GetComponentsInChildren <MyAudio> ();
	}

	protected virtual void Start ()
	{
		LevelSelect.Instance.levelSelectedEvent += OnLevelSelected;
//		if (levelIndex > GameController.Instance.unlockLevel) {
//			for (int i = 0; i < sprites.Length; i++) {
//				sprites [i].material = greyMat;
//				sprites [i].color = new Color (0.8f, 0.8f, 0.8f, 1);
//			}
//		}
	}

	public virtual void OnMouseDown ()
	{
		if (GameController.Instance.unlockLevel < levelIndex)
			return;
		if (isSelected) {
			transform.localScale = new Vector3 (1.1f, 1.1f, 1);
			for (int i = 0; i < animators.Length; i++) {
				animators [i].enabled = true;
			}
			for (int i = 0; i < myaudios.Length; i++) {
				myaudios [i].Play ();
			}
		}
	}

	public virtual void OnMouseExit ()
	{
		transform.localScale = new Vector3 (1, 1, 1);
		for (int i = 0; i < animators.Length; i++) {
			if (animators [i].gameObject.name != "eye")
				animators [i].enabled = false;
		}
		for (int i = 0; i < myaudios.Length; i++) {
			myaudios [i].StopAudio ();
		}
	}

	public virtual void OnMouseUp ()
	{
		transform.localScale = new Vector3 (1, 1, 1);
		for (int i = 0; i < animators.Length; i++) {
			if (animators [i].gameObject.name != "eye")
				animators [i].enabled = false;
		}
		for (int i = 0; i < myaudios.Length; i++) {
			myaudios [i].StopAudio ();
		}
	}

	IEnumerator OnMouseUpAsButton ()
	{
		transform.localScale = new Vector3 (1, 1, 1);
		for (int i = 0; i < myaudios.Length; i++) {
			myaudios [i].StopAudio ();
		}
		if (isSelected && Mathf.Abs (LevelSelect.Instance.levelPositions [levelIndex] - LevelSelect.Instance.transform.position.x) < 0.03) {
			GameController.Instance.currentLevel = levelIndex;
			parentAudio.Play ();
			transform.Find ("eye").GetComponent<Animator> ().SetTrigger ("Trans");
			yield return new WaitForSeconds (0.8f);
			SceneManager.LoadScene ("Level" + (levelIndex + 1));
		}
	}

	void OnLevelSelected (int i)
	{
		if (i == levelIndex) {
			isSelected = true;
			Selected ();
		} else if (isSelected) {
			UnSelected ();
			isSelected = false;
		}
	}

	public virtual void Selected ()
	{
		halo.transform.localScale = Vector3.Lerp (halo.transform.localScale, Vector3.one, Time.deltaTime);
		for (int i = 0; i < sprites.Length; i++) {
			sprites [i].color = new Color (1, 1, 1, 1);
		}
	}

	public virtual void UnSelected ()
	{
		halo.transform.localScale = Vector3.Lerp (halo.transform.localScale, Vector3.zero, Time.deltaTime);
		for (int i = 0; i < sprites.Length; i++) {
			sprites [i].color = new Color (0.7f, 0.7f, 0.7f, 0.9f);
		}
	}
}
