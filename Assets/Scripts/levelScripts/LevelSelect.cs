﻿using UnityEngine;
using System;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;

public class LevelSelect : SceneSingleton<LevelSelect>
{
	public Transform backGround;
	public int curntSelct = 0;
	public float[] levelPositions;
	public float[] backgroundPositions;
	public float levelSettingPosition;
	bool canMoveNext = true;
	bool canMovePre = true;

	public const float AndroidMoveSensitive = 0.01f;
	public const float IOSMoveSensitive = 0.0018f;
	public const float AndroidSlideSensitive = 3000;
	public const float IOSSlideSensitive = 30000;
	float moveSensitive;
	float slideSensitive;

	public event Action<int> levelSelectedEvent;

	public bool inSetting = false;

	public bool InSetting {
		get {
			return inSetting;
		}
		set {
			inSetting = value;
		}
	}

	int trueUnlocked = 0;

	MyAudio myaudio;

	public Button back;
	public Button setting;

	void Awake ()
	{
		myaudio = GetComponent <MyAudio> ();
	}

	void Start ()
	{
		curntSelct = GameController.Instance.currentLevel;
		transform.position = new Vector3 (levelPositions [curntSelct], transform.position.y, 0);
		levelSelectedEvent (curntSelct);
		switch (Application.platform) {
		case RuntimePlatform.Android:
			moveSensitive = AndroidMoveSensitive;
			slideSensitive = AndroidSlideSensitive;
			break;
		case RuntimePlatform.IPhonePlayer:
			moveSensitive = IOSMoveSensitive;
			slideSensitive = IOSSlideSensitive;
			break;
		default:
			moveSensitive = AndroidMoveSensitive;
			slideSensitive = AndroidSlideSensitive;
			break;
		}
		trueUnlocked = GameController.Instance.unlockLevel;
		if (trueUnlocked > 3)
			trueUnlocked = 3;
	}
	// Update is called once per frame
	void FixedUpdate ()
	{
//		if (setting) {
//			if (transform.position.x < levelSettingPosition) {
//				transform.position += new Vector3 (20f * Time.deltaTime, 0, 0);
//			}
//			return;
//		} else {
//			if (transform.position.x > levelPositions [curntSelct]) {
//				transform.position -= new Vector3 (20f * Time.deltaTime, 0, 0);
//			}
//		}
		backGround.position = new Vector2 (Mathf.Lerp (backgroundPositions [3], backgroundPositions [0], (transform.position.x - levelPositions [3]) / (levelPositions [0] - levelPositions [3])), backGround.position.y);
		if (Input.touchCount > 0) {
			if (Input.GetTouch (0).phase == TouchPhase.Moved) {
				if (transform.position.x < levelPositions [0] + 2 - moveSensitive * Input.GetTouch (0).deltaPosition.x && transform.position.x > levelPositions [trueUnlocked] - 2 - moveSensitive * Input.GetTouch (0).deltaPosition.x) {
					transform.position += new Vector3 (moveSensitive * Input.GetTouch (0).deltaPosition.x, 0, 0);
					if (Input.GetTouch (0).deltaPosition.x < -slideSensitive * moveSensitive && canMoveNext) {
						if (curntSelct < trueUnlocked) {
							transform.position = Vector2.Lerp (transform.position, new Vector2 (levelPositions [curntSelct + 1], transform.position.y), 20 * Time.deltaTime);
							curntSelct++;
							levelSelectedEvent (curntSelct);
							myaudio.Play ();
							canMoveNext = false;
							canMovePre = true;
						}
					}
					if (Input.GetTouch (0).deltaPosition.x > slideSensitive * moveSensitive && canMovePre) {
						if (curntSelct > 0) {
							transform.position = Vector2.Lerp (transform.position, new Vector2 (levelPositions [curntSelct - 1], transform.position.y), 20 * Time.deltaTime);
							curntSelct--;
							levelSelectedEvent (curntSelct);
							myaudio.Play ();
							canMoveNext = true;
							canMovePre = false;
						}
					}
				} 
			}
			return;
		}
		if (!inSetting) {
			transform.position = Vector2.Lerp (transform.position, new Vector2 (levelPositions [curntSelct], transform.position.y), 20 * Time.deltaTime);
			canMoveNext = true;
			canMovePre = true;
		}
	}

	public void Settings ()
	{
//			SceneManager.LoadScene ("Settings");
		StartCoroutine (SettingAnim ());
	}

	public void SettingBack ()
	{
		StartCoroutine (BackAnim ());
	}

	IEnumerator SettingAnim ()
	{
		InSetting = true;
		while (transform.position.x < levelSettingPosition) {
			transform.position += new Vector3 ((levelSettingPosition - levelPositions [curntSelct]) * Time.deltaTime, 0, 0);
			yield return null;
		} 
		back.interactable = true;
	}

	IEnumerator BackAnim ()
	{
		InSetting = true;
		while (transform.position.x > levelPositions [curntSelct]) {
			transform.position -= new Vector3 ((levelSettingPosition - levelPositions [curntSelct]) * Time.deltaTime, 0, 0);
			yield return null;
		}
		setting.interactable = true;
		InSetting = false;
	}

	public void Home ()
	{
		SceneManager.LoadScene ("Start");
	}

	public void Tutorial ()
	{
		SceneManager.LoadScene ("Tutorial");
	}

}
