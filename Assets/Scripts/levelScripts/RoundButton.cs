﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[SerializeField]
public enum RoundState
{
	Completed = 0,
	Current = 1,
	Locked = 2
}

public class RoundButton : MonoBehaviour
{
	public int roundIndex;
	public RoundState state;
	SpriteRenderer sprite;
	Color spriteColor;
	Sprite basicSprite;
	AudioSource audioSource;
	MyAudio myAudio;

	void Awake ()
	{
		sprite = GetComponent <SpriteRenderer> ();
		spriteColor = sprite.color;
		audioSource = gameObject.AddComponent <AudioSource> ();
		audioSource.playOnAwake = false;
		audioSource.clip = RoundSelect.Instance.buttonClip;
		myAudio = gameObject.AddComponent <MyAudio> ();
		myAudio.audioSource = audioSource;
	}
	// Use this for initialization
	void Start ()
	{
		if (GameController.Instance.currentLevel < GameController.Instance.unlockLevel) {
			state = RoundState.Completed;
		} else if (GameController.Instance.currentLevel == GameController.Instance.unlockLevel) {
			if (roundIndex < GameController.Instance.unlockRound)
				state = RoundState.Completed;
			else if (roundIndex == GameController.Instance.unlockRound)
				state = RoundState.Current;
			else
				state = RoundState.Locked;
		}
		switch (state) {
		case RoundState.Locked:
			sprite.sprite = RoundSelect.Instance.lockedButn;
			break;
		case RoundState.Current:
			sprite.sprite = RoundSelect.Instance.curntButn;
			break;
		case RoundState.Completed:
			sprite.sprite = RoundSelect.Instance.completedButn;
			break;
		}
		basicSprite = sprite.sprite;
		if (state == RoundState.Current) {
			Instantiate (RoundSelect.Instance.ButtonEffect, transform.position, Quaternion.identity);
		} else {
			Transform effect = transform.FindChild ("Button(clone)");
			if (effect != null) {
				Destroy (effect.gameObject);
			}
		}
	}

	void OnMouseDown ()
	{
		if (state == RoundState.Completed) {
			sprite.sprite = RoundSelect.Instance.pressedCoompleted;
			myAudio.Play ();
		} else if (state == RoundState.Current) {
			sprite.sprite = RoundSelect.Instance.pressedCurnt;
			myAudio.Play ();
		}
	}

	void OnMouseExit ()
	{
		sprite.sprite = basicSprite;
	}

	void OnMouseUpAsButton ()
	{
		if (GameController.Instance.currentLevel == 0 && roundIndex == 0) {
			if (PlayerPrefs.GetInt ("First") != 1) {
				PlayerPrefs.SetInt ("Explode", 1);
				PlayerPrefs.SetInt ("Antibiotic", 1);
				PlayerPrefs.SetInt ("UlRay", 1);
				PlayerPrefs.SetInt ("First", 1);
				SceneManager.LoadScene ("Tutorial");
				return;
			}
		}
		GameController.Instance.currentRound = roundIndex;
		if (state != RoundState.Locked) {
			SceneManager.LoadScene (RoundSelect.Instance.mainScene);
		}
	}
}
