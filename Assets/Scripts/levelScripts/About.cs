﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class About : MonoBehaviour
{
	public void Back ()
	{
		SceneManager.LoadScene ("Start");
	}

	public void Appraise ()
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			Application.OpenURL ("https://itunes.apple.com/app/id1107339476");
		}
		if (Application.platform == RuntimePlatform.Android) {
			if (Application.systemLanguage == SystemLanguage.Chinese) {
				Application.OpenURL ("http://zhushou.360.cn/detail/index/soft_id/3281659");
			} else {
				Application.OpenURL ("https://play.google.com/store/apps/details?id=com.liyumeng.weround");
			}
		}
	}
}
