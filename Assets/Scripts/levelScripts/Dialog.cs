﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Dialog : SceneSingleton<Dialog>
{
	string text1 = "让我们一起来商量一下对战策略吧！";
	string text2 = "连接三个抗体将病毒包围起来，抗体闭合时必须要连接到最初的抗体，才能将病毒消灭哟！";
	string text3 = "消灭病毒会产生蛋白质，积累一定数量蛋白质按下右上角按钮可以增援抗体。增援所需蛋白质会不断增加";
	string text4 = "现在让我们连接更多的抗体，来消灭更多的病毒吧! 送你三个秘密武器，记得在道具栏使用哦！";

	int step = 1;

	public Text textUI;
	public Text protein;
	public Button OK;
	public GameObject tutor1;
	public GameObject tutor2;
	public GameObject tutor3;
	string text;

	void Awake ()
	{
		if (Application.systemLanguage != SystemLanguage.Chinese && Application.systemLanguage != SystemLanguage.ChineseSimplified && Application.systemLanguage != SystemLanguage.ChineseTraditional) {
			text1 = "Let's discuss how to defeat viruses!";
			text2 = "Connect three or more antibodies end to end,        and destroy the viruses!";
			text3 = "Then can reproduce antibodies by pressing the button (with increasing consumption).";
			text4 = "Try to destroy more viruses! Give you three props. Remember to use!";
		}
	}

	void Start ()
	{
		step = 1;
		text = text1;
		OK.gameObject.SetActive (false);
		StartCoroutine (TextDisplay ());
		protein.text = "0";
	}

	public void NextDialog ()
	{
		switch (step) {
		case 1:
			text = text2;
			tutor1.SetActive (true);
			step = 2;
			break;
		case 2:
			text = text3;
			protein.text = "20";
			tutor1.SetActive (false);
			tutor2.SetActive (true);
			step = 3;
			break;
		case 3:
			text = text4;
			StartCoroutine (ProteinDelayShow ());
			tutor2.SetActive (false);
			tutor3.SetActive (true);
			step = 4;
			break;
		case 4:
			SceneManager.LoadScene ("LevelsSelect");
			break;
//		default:
//			OK.gameObject.SetActive (false);
//			return;
		}
		StopCoroutine (TextDisplay ());
		StartCoroutine (TextDisplay ());
	}

	IEnumerator ProteinDelayShow ()
	{
		yield return new WaitForSeconds (1.6f);
		protein.text = "0";
	}

	IEnumerator TextDisplay ()
	{
		textUI.text = "";
		OK.gameObject.SetActive (false);
		for (int i = 0; i < text.Length; i++) {
			yield return new WaitForSeconds (0.08f);
			textUI.text += text [i];
		}
		if (text == text1)
			OK.gameObject.SetActive (true);
	}

	public void Back ()
	{
		SceneManager.LoadScene ("LevelsSelect");
	}
}
