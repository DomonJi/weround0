﻿using UnityEngine;
using UnityEngine.UI;

public class WaveSlider : MonoBehaviour
{
	public GameObject[] flags;
	[SerializeField]Color[] colors;
	[SerializeField]Image fill;
	[SerializeField]Sprite[] sprites;
	Slider slider;

	void Awake ()
	{
		slider = GetComponent <Slider> ();
	}

	void OnEnable ()
	{
		flags [ObjectManager.Instance.roundItem.waves.Length - 2].SetActive (true);
		Image[] virusFlags = flags [ObjectManager.Instance.roundItem.waves.Length - 2].GetComponentsInChildren<Image> ();
		for (int i = 0; i < virusFlags.Length; i++) {
			virusFlags [i].sprite = sprites [GameController.Instance.currentLevel];
		}
		fill.color = colors [GameController.Instance.currentLevel];
	}

	void FixedUpdate ()
	{
		if (Time.timeSinceLevelLoad < 3.5 + ObjectManager.Instance.virusSpawnDelay) {
			slider.value += ObjectManager.Instance.virusSpawnDelay * Time.deltaTime / (ObjectManager.Instance.virusSpawnDelay * ObjectManager.Instance.roundItem.waves.Length) / (3.5f + ObjectManager.Instance.virusSpawnDelay);
		} else {
			slider.value += Time.deltaTime / (ObjectManager.Instance.virusSpawnDelay * ObjectManager.Instance.roundItem.waves.Length);
		}
	}
}
