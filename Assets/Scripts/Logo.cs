﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Logo : MonoBehaviour
{
	[SerializeField]SpriteRenderer carp;

	void Start ()
	{
		StartCoroutine (Animte ());
	}

	public void LoadStart ()
	{
		SceneManager.LoadScene ("CG");
	}

	IEnumerator Animte ()
	{
		while (carp.color.a < 1) {
			yield return new WaitForEndOfFrame ();
			carp.color += new Color (0, 0, 0, 0.4f * Time.deltaTime);
		}
		//yield return new WaitForSeconds (2f);
		GetComponent<Animator> ().enabled = true;
	}

	public void LogoAudio ()
	{
		GetComponent<AudioSource> ().Play ();
	}
}
