﻿using UnityEngine;
using System.Collections;

public class CommonVirus : Virus
{
	protected override void OnEnable ()
	{
		base.OnEnable ();
		if (Random.value > 0.3)
			transform.FindChild ("Expression").GetComponent<Animator> ().SetTrigger ("TwoEyes");
	}

}
