﻿using UnityEngine;
using System.Collections;

public class CamouflageVirus : Virus
{
	[SerializeField]float timeToCamou;
	public bool inCamou;

	public override void Die ()
	{
		if (!canEquipShield || canBeDestroyed) {
			if (!inCamou) {
				if (SceneController.Instance)
					SceneController.Instance.virus.Remove (this);
				rgbody.isKinematic = true;
				movement.enabled = false;
				collider.enabled = false;
				rgbody.Sleep ();
				StopAllCoroutines ();
				base.DisableChildren ();
				InstantiateParticle ();
				animator.SetTrigger ("Die");
			}
		} else {
			DisableShield ();
		}
	}

	protected override void OnEnable ()
	{
		base.OnEnable ();
		transform.FindChild ("Expression").gameObject.SetActive (true);
		StartCoroutine (Camouflage ());
	}

	protected override void Init ()
	{
		canBeDestroyed = true;
		level = 1;
		timeToCamou = 10f;
	}

	IEnumerator Camouflage ()
	{
		while (true) {
			yield return new WaitForSeconds (timeToCamou + Random.Range (-2, 1));
			if (!ItemManager.Instance.inUlray) {
				Camou ();
				yield return new WaitForSeconds (2 * timeToCamou + Random.Range (-1, 3));
				DisCamou ();
			}
		}
	}

	public override void Push ()
	{
		ObjectManager.Instance.Push ("CamouflagVirus", gameObject);
	}

	void Camou ()
	{
		//canBeDestroyed = false;
		transform.FindChild ("Expression").gameObject.SetActive (false);
		animator.SetBool ("IsCamou", true);
	}

	void DisCamou ()
	{
		//canBeDestroyed = true;
		animator.SetBool ("IsCamou", false);
		StartCoroutine (DelayAddFace ());
	}

	IEnumerator DelayAddFace ()
	{
		yield return new WaitForSeconds (1.5f);
		AddFace ();
	}

	public override void DisableFeature ()
	{
		base.DisableFeature ();
		DisCamou ();
	}

	public override void EnableFeatue ()
	{
		base.DisableFeature ();
	}

	public void CamouEvent ()
	{
		if (animator.GetBool ("IsCamou")) {
			inCamou = true;
			movement.CanMove = false;
		} else {
			inCamou = false;
			if (!ItemManager.Instance.inUlray)
				movement.CanMove = true;
		}
	}

	public void AddFace ()
	{
		if (!animator.GetBool ("IsCamou"))
			transform.FindChild ("Expression").gameObject.SetActive (true);
	}

	protected override void InstantiateParticle ()
	{
		ObjectManager.Instance.ProduceParticles ("CamouPar", transform.position);
	}
}
