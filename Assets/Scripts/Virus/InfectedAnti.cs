﻿using UnityEngine;
using System.Collections;

public class InfectedAnti : Virus
{
	protected override void Init ()
	{
		level = -1;
		canBeDestroyed = true;
	}

	public override void Push ()
	{
		ObjectManager.Instance.Push ("InfectedAnti", gameObject);
	}

	protected override void OnEnable ()
	{
		base.OnEnable ();
		if (Random.value > 0.3)
			transform.FindChild ("Expression").GetComponent<Animator> ().SetTrigger ("TwoEyes");
	}
}
