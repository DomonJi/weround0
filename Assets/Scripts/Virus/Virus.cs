﻿using UnityEngine;
using System.Collections;

public class Virus : MonoBehaviour
{
	protected Animator animator;
	protected Animator shieldAnimator;
	public Movement movement;
	protected Collider2D collider;
	protected Rigidbody2D rgbody;

	public int level;

	public bool canBeDestroyed;
	public bool canEquipShield;
	public int shieldRecoverTime = 10;
	public GameObject shield;

	void Awake ()
	{
		Init ();
		movement = GetComponent<VirusMovement> ();
		animator = GetComponent<Animator> ();
		collider = GetComponent<Collider2D> ();
		rgbody = GetComponent<Rigidbody2D> ();
	}

	protected virtual void OnEnable ()
	{
		SceneController.Instance.virus.Add (this);
		rgbody.isKinematic = false;
		GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 1);
		movement.enabled = true;
		collider.enabled = true;
		EnableChildren ();
		if (canEquipShield) {
			UnEquipShield ();
		}
		Transform effect = transform.FindChild ("UlrayVEffect(Clone)");
		if (effect != null) {
			Destroy (effect.gameObject);
		}
		movement.CanMove = true;
		rgbody.WakeUp ();
	}

	protected virtual void OnMouseDown ()
	{
		if (SceneController.Instance.canVBoom) {
			Explode ();
			ProteinManager.Instance.CalcuProtein (new []{ this });
			SceneController.Instance.CheckGameOver ();
		}
	}

	public virtual void EquipShield ()
	{
		canEquipShield = true;
		canBeDestroyed = false;
		shield = Instantiate (ObjectManager.Instance.shieldPfb, transform.position, Quaternion.identity)as GameObject;
		shield.transform.SetParent (transform);
		shieldAnimator = shield.GetComponent <Animator> ();
	}

	public void UnEquipShield ()
	{
		canEquipShield = false;
		canBeDestroyed = true;
		shield = transform.FindChild ("shield(Clone)").gameObject;
		Destroy (shield);
	}

	protected virtual void Init ()
	{
		level = 1;
		canEquipShield = false;
		canBeDestroyed = true;
	}

	public virtual void Die ()
	{
		if (!canEquipShield || canBeDestroyed) {
			if (SceneController.Instance)
				SceneController.Instance.virus.Remove (this);
			rgbody.isKinematic = true;
			movement.enabled = false;
			collider.enabled = false;
			rgbody.Sleep ();
			DisableChildren ();
			StopAllCoroutines ();
			animator.SetTrigger ("Die");
			InstantiateParticle ();
		} else {
			DisableShield ();
		}
	}

	protected void DisableChildren ()
	{
		for (int i = 0; i < transform.childCount; i++) {
			transform.GetChild (i).gameObject.SetActive (false);
		}
	}

	void EnableChildren ()
	{
		for (int i = 0; i < transform.childCount; i++) {
			transform.GetChild (i).gameObject.SetActive (true);
		}
	}

	protected virtual void Explode ()
	{
		if (!canEquipShield || canBeDestroyed) {
			if (SceneController.Instance)
				SceneController.Instance.virus.Remove (this);
			rgbody.isKinematic = true;
			movement.enabled = false;
			collider.enabled = false;
			rgbody.Sleep ();
			DisableChildren ();
			StopAllCoroutines ();
			animator.SetTrigger ("BombItem");
			InstantiateParticle ();
		} else {
			DisableShield ();
		}
	}

	protected virtual void InstantiateParticle ()
	{
		//Instantiate (ObjectManager.Instance.normalPar, transform.position, Quaternion.identity);
		ObjectManager.Instance.ProduceParticles ("NormalPar", transform.position);
	}

	void OnDisable ()
	{
		if (SceneController.Instance) {
			if (SceneController.Instance.virus.Contains (this))
				SceneController.Instance.virus.Remove (this);
		}
	}

	public virtual void Push ()
	{
		ObjectManager.Instance.Push ("Virus", gameObject);
	}

	public virtual void EnableShield ()
	{
		if (canEquipShield) {
			canBeDestroyed = false;
			level = 1;
			Debug.Log (shieldAnimator);
		}
	}

	public virtual void DisableShield ()
	{
		if (canEquipShield) {
			canBeDestroyed = true;
			level = 0;
			Debug.Log (shieldAnimator);
			shieldAnimator.SetTrigger ("Shield");
			shieldAnimator.SetBool ("HasShield", false);
			StartCoroutine (RecoverShield ());
		}
	}

	public virtual void DisableFeature ()
	{
		if (canEquipShield) {
			DisableShield ();
		}
	}

	public virtual void EnableFeatue ()
	{
		if (canEquipShield) {
			StartCoroutine (RecoverShield ());
		}
	}

	IEnumerator RecoverShield ()
	{
		while (true) {
			yield return new WaitForSeconds (shieldRecoverTime);
			if (!ItemManager.Instance.inUlray) {
				shieldAnimator.SetTrigger ("Shield");
				shieldAnimator.SetBool ("HasShield", true);
				yield break;
			}
		}
	}
}
