﻿using UnityEngine;
using System.Collections;

public class LittleVirus : Virus
{
	public override void Push ()
	{
		ObjectManager.Instance.Push ("LittleVirus", gameObject);
	}

	protected override void Init ()
	{
		level = 4;
		canBeDestroyed = true;
	}

	protected override void InstantiateParticle ()
	{
		ObjectManager.Instance.ProduceParticles ("BigPar", transform.position);
	}
}
