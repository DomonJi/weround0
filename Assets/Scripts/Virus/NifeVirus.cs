﻿using UnityEngine;
using System.Collections;

public class NifeVirus : Virus
{

	public GameObject nifeNife;
	Line linetouched;
	[SerializeField]Animator immune;
	bool canKnife = true;

	public override void Push ()
	{
		ObjectManager.Instance.Push ("NifeVirus", gameObject);
	}

	protected override void Init ()
	{
		level = 3;
		canBeDestroyed = true;
	}

	protected override void OnMouseDown ()
	{
		if (SceneController.Instance.canVBoom) {
			if (canEquipShield && !canBeDestroyed)
				DisableShield ();
			else
				immune.SetTrigger ("Immune");
		}
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (!canKnife) {
			return;
		}
		Debug.Log (other.gameObject.name);
		if (other.gameObject.name == "Line") {
			GameObject nife = Instantiate (nifeNife, transform.position, Quaternion.identity) as GameObject;
			nife.transform.SetParent (transform);
			linetouched = other.gameObject.GetComponent<Line> ();
			linetouched.knifeAnnim.SetTrigger ("Knife");
//			other.gameObject.GetComponent <Line> ().IsActive = false;
//			other.gameObject.GetComponent <Line> ().IsCollider = false;
//			SceneController.Instance.EndSelection ();
		}
	}

	public void NifeNife ()
	{
		linetouched.StartBroke ();
	}

	protected override void InstantiateParticle ()
	{
		ObjectManager.Instance.ProduceParticles ("NifePar", transform.position);
	}

	public override void DisableFeature ()
	{
		base.DisableFeature ();
		canKnife = false;
	}

	public override void EnableFeatue ()
	{
		base.EnableFeatue ();
		canKnife = true;
	}
}
