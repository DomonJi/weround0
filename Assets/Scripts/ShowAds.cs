﻿using UnityEngine;
using UnityEngine.Advertisements;
using System.Collections;
using UnityEngine.UI;


[SerializeField]
public enum AdsType
{
	RANDOM,
	DEFAULT,
	DELAY
}

public class ShowAds : MonoBehaviour
{
	public AdsType adstype;
	Coroutine delayShow;
	[SerializeField]Text text;

	public void ShowAd ()
	{
		if (Advertisement.IsReady () && Application.platform == RuntimePlatform.IPhonePlayer) {
			Advertisement.Show ();
		}
	}
			
	//
	//	void FixedUpdate ()
	//	{
	//		if (!showed) {
	//			ShowAd ();
	//			showed = true;
	//		}
	//	}

	public void Show ()
	{
		if (adstype == AdsType.RANDOM) {
			if (Random.value > 0.75 && GameController.Instance.currentRound > 0) {
				Show ();
			}
		}
		if (adstype == AdsType.DEFAULT) {
			ShowAd ();
		}
	}

	public void CancelAd ()
	{
//		Baidu.Instance ().removeBanner ();
	}

	public void DelayShowAd ()
	{
		delayShow = StartCoroutine (DelayShow ());
	}

	public void StopDelayShow ()
	{
		StopCoroutine (delayShow);
	}

	IEnumerator DelayShow ()
	{
		float timer = Time.realtimeSinceStartup;
		yield return new WaitUntil (() => Time.realtimeSinceStartup - timer > 6);
		ShowAd ();
	}

	public void ShowRewardedAd ()
	{
		if (Advertisement.IsReady ("rewardedVideo")) {
			var options = new ShowOptions { resultCallback = HandleShowResult };
			Advertisement.Show ("rewardedVideo", options);
			return;
		} else {
			Debug.Log ("Failed"); 
			GameObject.Find ("FailedHint4").GetComponent <Animator> ().SetTrigger ("Show");
			return;
		}
	}

	private void HandleShowResult (ShowResult result)
	{
		switch (result) {
		case ShowResult.Finished:
			Debug.Log ("The ad was successfully shown.");
			//
			// YOUR CODE TO REWARD THE GAMER
			PlayerPrefs.SetInt ("Gold", PlayerPrefs.GetInt ("Gold") + 200);
			text.text = PlayerPrefs.GetInt ("Gold").ToString ();
			GetComponent <MyAudio> ().Play ();
			// Give coins etc.
			break;
		case ShowResult.Skipped:
			Debug.Log ("The ad was skipped before reaching the end.");
			break;
		case ShowResult.Failed:
			Debug.LogError ("The ad failed to be shown.");
			break;
		}
	}

}