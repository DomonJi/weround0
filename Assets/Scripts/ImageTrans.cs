﻿using UnityEngine;
using System.Collections;

public class ImageTrans : MonoBehaviour
{
	public Sprite sprite;
	// Use this for initialization
	void Awake ()
	{
		if (Application.systemLanguage != SystemLanguage.Chinese && Application.systemLanguage != SystemLanguage.ChineseSimplified && Application.systemLanguage != SystemLanguage.ChineseTraditional) {
			GetComponent<SpriteRenderer> ().sprite = sprite;
		}
	}
}
