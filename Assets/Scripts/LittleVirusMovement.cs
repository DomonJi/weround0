﻿using UnityEngine;
using System.Collections;

public class LittleVirusMovement : VirusMovement
{
	protected override void RandomMove ()
	{
		StartCoroutine (FastMove ());
	}

	IEnumerator FastMove ()
	{
		LittleRandomMove ();
		yield return new WaitForSeconds (SceneController.Instance.tickInterval / 2);
		LittleRandomMove ();
	}

	void LittleRandomMove ()
	{
		if (CanMove) {
			animator.SetTrigger ("Move");
			distanceFormCenter = SceneController.Instance.centerOfAnti - (Vector2)transform.position;
			float lengthFromCenter = distanceFormCenter.magnitude;
			if (lengthFromCenter > 5) {
				if (Random.value < 0.3) {
					rgbody.MoveRotation (transform.eulerAngles.z + Vector2.Angle (transform.TransformDirection (-Vector3.right), distanceFormCenter));
				} else {
					rgbody.MoveRotation (Random.Range (0f, 360f));
				}
			} else if (lengthFromCenter < 4) {
				if (Random.value < 0.4) {
					rgbody.MoveRotation (transform.eulerAngles.z + Vector2.Angle (transform.TransformDirection (-Vector3.right), -distanceFormCenter));
				} else {
					rgbody.MoveRotation (Random.Range (0f, 360f));
				}
			} 
		}
	}
}
