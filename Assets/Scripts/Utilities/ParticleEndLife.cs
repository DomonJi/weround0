﻿using UnityEngine;
using System.Collections;

public class ParticleEndLife : MonoBehaviour
{
	public string parName;
	// Use this for initialization
	void Start ()
	{
		Invoke ("Push", 1.5f);
	}

	void Push ()
	{
		ObjectManager.Instance.Push (parName, gameObject);
	}
}
