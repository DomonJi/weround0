﻿using UnityEngine;
using System.Collections;

//using cn.sharesdk.unity3d;

public class Share : MonoBehaviour
{
	//	ShareSDK ssdk = new ShareSDK ();
	//	ShareContent content = new ShareContent ();
	//
	//	void Awake ()
	//	{
	//		content.SetText ("this is a test string.");
	//		content.SetImageUrl ("https://f1.webshare.mob.com/code/demo/img/1.jpg");
	//		content.SetTitle ("test title");
	//		content.SetShareType (ContentType.App);
	//		ssdk.shareHandler = ShareResultHandler;
	//		ssdk.authHandler = AuthResultHandler;
	//	}
	//
	//	public void ShowShare ()
	//	{
	//		try {
	//			ssdk.ShareContent (PlatformType.QQPlatform, content);
	//		} catch {
	//			Application.OpenURL ("http://m.weibo.cn/");
	//		}
	//
	//	}
	//
	//
	//	void ShareResultHandler (int reqID, ResponseState state, PlatformType type, Hashtable result)
	//	{
	//		if (state == ResponseState.Success) {
	//			print ("share result :");
	//			print (MiniJSON.jsonEncode (result));
	//		} else if (state == ResponseState.Fail) {
	//			print ("fail! error code = " + result ["error_code"] + "; error msg = " + result ["error_msg"]);
	//		} else if (state == ResponseState.Cancel) {
	//			print ("cancel !");
	//		}
	//	}
	//
	//	void AuthResultHandler (int reqID, ResponseState state, PlatformType type, Hashtable result)
	//	{
	//		if (state == ResponseState.Success) {
	//			print ("authorize success !");
	//		} else if (state == ResponseState.Fail) {
	//			print ("fail! error code = " + result ["error_code"] + "; error msg = " + result ["error_msg"]);
	//		} else if (state == ResponseState.Cancel) {
	//			print ("cancel !");
	//		}
	//	}

	Platform[] platforms = {
		Platform.QQ,
		Platform.QZONE,
		Platform.TENCENT_WEIBO,
		Platform.SINA,
		Platform.TWITTER,
		Platform.INSTAGRAM,
		Platform.EMAIL,
		Platform.RENREN,
		Platform.SMS,
		Platform.DOUBAN,
		Platform.WEIXIN,
		Platform.WEIXIN_CIRCLE
	};

	void Awake ()
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			Social.SetAppKey ("574ae25d67e58e3f98000876");
		} else {
			Social.SetAppKey ("574ae27767e58e36520001c5");
		}
		Social.SetQQAppIdAndAppKey ("1105281119", "6rOdL7ArwevoCQUt");
		Social.SetTargetUrl ("http://mp.weixin.qq.com/s?__biz=MzIxMTQwMjEzOQ==&mid=100000002&idx=1&sn=cfb0b5bb0ab0f1e1e5e7b7a8502ac591#rd");
		Social.SetWechatAppIdAndSecret ("wx4ce8b3e8a3b48fbc", "bc3168ac9f2a72ffafb290d2d2b158f3");
		Social.OpenInstagram ();
		Social.OpenTwitter ();
	}

	public void OpenShare ()
	{
		Social.OpenShareWithImagePath (platforms, "一款画面精美的连线消除游戏！让WEROUND带你一起穿越时空、踏上冒险之旅！青春就是要浪费在游戏上！", "http://mmbiz.qpic.cn/mmbiz/Kib0a9bkj160UwoYVLcm5ibg22Qsb2tQL6L8ZnqytGHATiahk9QTov5RLA54wpDicfanvkQnlUQJr8wia0kcxxvBGDg/640?wx_fmt=jpeg&tp=webp&wxfrom=5&wx_lazy=1",
			delegate(Platform platform, int stCode, string errorMsg) {
				if (stCode == Social.SUCCESS) {
					Debug.Log ("分享成功");
				} else {
					Debug.Log ("分享失败" + errorMsg);
				}
			});
	}
}
