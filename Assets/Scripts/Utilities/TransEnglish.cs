﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TransEnglish : MonoBehaviour
{
	public string english;
	// Use this for initialization
	void Awake ()
	{
		if (Application.systemLanguage != SystemLanguage.Chinese && Application.systemLanguage != SystemLanguage.ChineseSimplified && Application.systemLanguage != SystemLanguage.ChineseTraditional) {
			GetComponent<Text> ().text = english;
		}
	}

}
