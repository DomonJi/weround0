﻿using UnityEngine;
using System.Collections;

public class Antibiotic : Item
{

	public override void Init ()
	{
		base.Init ();
		cd = 25f;
		duration = 10f;
	}

	protected override void Effect ()
	{
		base.Effect ();
		StartCoroutine (Antibio ());
	}

	IEnumerator Antibio ()
	{
		SceneController.Instance.canVInfect = false;
		SceneController.Instance.antiVirus.ForEach (a => {
			a.warningAnimator.SetTrigger ("Cancle");
			a.warningAnimator.SetBool ("Protected", true);
		});
		yield return new WaitForSeconds (duration);
		SceneController.Instance.antiVirus.ForEach (a => a.warningAnimator.SetBool ("Protected", false));
		SceneController.Instance.canVInfect = true;
	}

	void Start ()
	{
		if (GameController.Instance.roundConfigs.levels [GameController.Instance.currentLevel] [GameController.Instance.currentRound].canVInfect < 1) {
			buttom.interactable = false;
		}
	}

	public override void Enable ()
	{
		buttom.interactable = num > 0 && Time.time > cdTimer + cd && GameController.Instance.roundConfigs.levels [GameController.Instance.currentLevel] [GameController.Instance.currentRound].canVInfect > 0;
	}
}
