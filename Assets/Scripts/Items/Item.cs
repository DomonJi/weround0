﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Item : MonoBehaviour
{
	[SerializeField]protected float cd;
	[SerializeField]protected bool condition;
	[SerializeField]protected int cost;
	[SerializeField]protected Button buttom;
	[SerializeField]protected float duration;
	public Text hintText;

	public int num;
	 
	protected float cdTimer;

	void Awake ()
	{
		Init ();
		buttom.interactable = num > 0 && condition;
	}

	void FixedUpdate ()
	{
		if (buttom.image.fillAmount < 1) {
			buttom.image.fillAmount += Time.deltaTime / (cd - duration);
		}
	}

	public void StartGame ()
	{
		cdTimer = -cd;
		GetComponentInChildren <Text> ().text = num.ToString ();
	}

	public virtual void Init ()
	{
		condition = true;
		num = PlayerPrefs.GetInt (GetType ().Name);
	}

	public void Trigge ()
	{
		if (num > 0) {
			cdTimer = Time.time;
			Effect ();
			StartCoroutine (StartCD ());
			ItemManager.Instance.DisableOthers ();
			StartCoroutine (ButtonDown ());
			hintText.gameObject.SetActive (true);
		}
	}

	IEnumerator ButtonDown ()
	{
		yield return new WaitUntil (() => Time.time > cdTimer + duration);
		buttom.image.fillAmount = 0;
		ItemManager.Instance.itemBar.SetTrigger ("ButtonMove");
		ItemManager.Instance.EnableOthers ();
		yield return new WaitForSeconds (1f);
		hintText.gameObject.SetActive (false);
	}

	protected virtual void Effect ()
	{
		num--;
		PlayerPrefs.SetInt (GetType ().Name, num);
		GetComponentInChildren <Text> ().text = num.ToString ();
	}

	protected virtual IEnumerator StartCD ()
	{
		buttom.interactable = false;
		yield return new WaitUntil (() => Time.time > cdTimer + cd);
		buttom.interactable = num > 0 && condition;
	}


	public void Disable ()
	{
		buttom.interactable = false;
	}

	public virtual void Enable ()
	{
		buttom.interactable = num > 0 && Time.time > cdTimer + cd;
	}
}
