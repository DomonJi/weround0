﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class CG : MonoBehaviour
{
	public AudioClip[] audioclips;
	public AudioSource audioSource;
	static int i = 0;

	public void OnMouseUp ()
	{
		SceneManager.LoadScene ("Start");
	}

	public void PlayAudio ()
	{
		audioSource.PlayOneShot (audioclips [i++]);
	}
}
