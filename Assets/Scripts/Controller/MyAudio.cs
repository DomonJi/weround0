﻿using UnityEngine;
using System.Collections;
using System.Xml.Serialization;

public class MyAudio : MonoBehaviour
{
	public enum AudioType
	{
		AUDIO,
		MUSIC
	}

	public AudioType audioType;
	public AudioSource audioSource;

	void Start ()
	{
		if (audioType == AudioType.AUDIO) {
			StartCoroutine (ListenAudio ());
		} else {
			if (PlayerPrefs.GetInt ("Music") < 1)
				audioSource.Play ();
			StartCoroutine (ListenMusic ());
		}

	}

	public void Play ()
	{
		if (audioType == AudioType.AUDIO) {
			if (PlayerPrefs.GetInt ("Audio") < 1) {
				audioSource.Play ();
			}
		} else {
			if (PlayerPrefs.GetInt ("Music") < 1) {
				audioSource.Play ();
			}
		}
	}

	public void StopAudio ()
	{
		if (audioType == AudioType.AUDIO)
			audioSource.Stop ();
	}

	public void StopMusic ()
	{
		if (audioType == AudioType.MUSIC)
			audioSource.Stop ();
	}

	public void PlayOneShot (AudioClip ac)
	{
		if (audioType == AudioType.AUDIO) {
			if (PlayerPrefs.GetInt ("Audio") < 1) {
				audioSource.PlayOneShot (ac);
			}
		}
	}

	IEnumerator ListenAudio ()
	{
		while (true) {
			yield return new WaitUntil (() => PlayerPrefs.GetInt ("Audio") == 1);
			if (audioType == AudioType.AUDIO) {
				if (audioSource != null)
					audioSource.Stop ();
			}
		}
	}

	IEnumerator ListenMusic ()
	{
		while (true) {
			yield return new WaitUntil (() => PlayerPrefs.GetInt ("Music") == 1);
			if (audioType == AudioType.MUSIC) {
				if (audioSource != null)
					audioSource.Stop ();
			}
			yield return new WaitUntil (() => PlayerPrefs.GetInt ("Music") < 1);
			if (audioType == AudioType.MUSIC) {
				if (audioSource != null)
					audioSource.Play ();
			}
		}
	}
}
