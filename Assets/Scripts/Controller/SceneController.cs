﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;


public class SceneController : SceneSingleton<SceneController>
{
	public List<AntiVirus> antiVirus;
	public List<AntiVirus> antiVirusOnLine;
	public List<Virus> virus;
	public List<Virus> virusToDie;
	//public List<AntiVirus> antiCanInfect;

	public bool startSelecting;
	public bool completeSelecting;
	public Vector2 centerOfAnti;
	public AntiVirus focusedAnti;

	[SerializeField]Camera mainCamera;
	[SerializeField]Canvas gameCanvas;
	[SerializeField]Canvas menuCanvas;
	[SerializeField]Canvas loseCanvas;
	[SerializeField]Canvas winCanvas;
	[SerializeField]Animator explodeEffect;
	public GameObject hints;
	[SerializeField]PolygonCollider2D checkInRangeCollider;

	[HideInInspector]public bool paused;
	[HideInInspector]public bool noVirusInScene;
	public bool canOperate = true;

	//List<Vector3> crossResults;
	//store the results of vector cross

	public delegate void TickHandler ();

	public event TickHandler tickEvent;

	public float tickInterval;
	float tickTimer;

	public bool canVInfect;
	public bool canVBoom;
	bool canTick;

	float timeToInfect;
	public float supposedTimeToInfect;
	float infectTimer;

	[SerializeField]float crazyTimer;

	bool endedOnce;

	[SerializeField]List<Transform> antiSpawnPos;
	[SerializeField]WaveSlider waveSlider;

	public AudioClip[] we;
	public AudioClip round;
	[SerializeField]AudioClip great;
	[SerializeField]AudioClip nice;
	[SerializeField]AudioClip awesome;
	[SerializeField]AudioClip unbelieveable;
	[SerializeField]AudioClip amazing;
	MyAudio myaudio;

	void Awake ()
	{
		myaudio = GetComponent <MyAudio> ();
		antiVirus = new List<AntiVirus> ();
		virus = new List<Virus> ();
		antiVirusOnLine = new List<AntiVirus> ();
		//crossResults = new List<Vector3> ();
		virusToDie = new List<Virus> ();
		virus = new List<Virus> ();
//		antiCanInfect = new List<AntiVirus> ();
		menuCanvas.gameObject.SetActive (false);
		checkInRangeCollider = GetComponent<PolygonCollider2D> (); 
		Init ();
		paused = true;
	}

	void Start ()
	{
		ItemManager.Instance.StartGame ();
		StartCoroutine (SpawnAnti ());
		waveSlider.enabled = true;
		StartCoroutine (StartGame ());
		endedOnce = false;
	}

	IEnumerator SpawnAnti ()
	{
		int antiNum = GameController.Instance.roundConfigs.levels [GameController.Instance.currentLevel] [GameController.Instance.currentRound].antiNum;
		for (int i = 0; i < antiNum; i++) {
			yield return new WaitForSeconds (2.5f / antiNum);
			Debug.Log (antiNum);
			int p = Random.Range (0, antiSpawnPos.Count);
			ObjectManager.Instance.SpawnAntiStart (antiSpawnPos [p].position * 1.5f).GetComponent <AntiVirus> ()
				.MoveTo (antiSpawnPos [p].position + new Vector3 (Random.Range (-1f, 1f), Random.Range (-1f, 0.4f)));
			antiSpawnPos.RemoveAt (p);
		}
	}

	public IEnumerator StartGame ()
	{
		yield return new WaitForSeconds (3.5f);
		canTick = true;
		paused = false;
		infectTimer = Time.time;
		canVInfect = GameController.Instance.roundConfigs.levels [GameController.Instance.currentLevel] [GameController.Instance.currentRound].canVInfect > 0;
		tickTimer = Time.time;
		StartCoroutine (Infect ());
		tickEvent += CalcuACenter;
		ItemManager.Instance.EnableOthers ();
		//antiCanInfect = new List<AntiVirus> (antiVirus);

		if (GameController.Instance.currentLevel == 0 && GameController.Instance.currentRound == 2) {
			hints.SetActive (true);
			hints.transform.GetChild (0).gameObject.SetActive (true);
			Time.timeScale = 0;
			paused = true;
		}
		if (GameController.Instance.currentLevel == 0 && GameController.Instance.currentRound == 3) {
			hints.SetActive (true);
			hints.transform.GetChild (1).gameObject.SetActive (true);
			Time.timeScale = 0;
			paused = true;
		}
		if (GameController.Instance.currentLevel == 1 && GameController.Instance.currentRound == 0) {
			hints.SetActive (true);
			hints.transform.GetChild (2).gameObject.SetActive (true);
			Time.timeScale = 0;
			paused = true;
		}
		if (GameController.Instance.currentLevel == 2 && GameController.Instance.currentRound == 0) {
			hints.SetActive (true);
			hints.transform.GetChild (3).gameObject.SetActive (true);
			Time.timeScale = 0;
			paused = true;
		}
		if (GameController.Instance.currentLevel == 3 && GameController.Instance.currentRound == 0) {
			hints.SetActive (true);
			hints.transform.GetChild (4).gameObject.SetActive (true);
			Time.timeScale = 0;
			paused = true;
		}
		yield return new WaitForSeconds (0.1f);
		ObjectManager.Instance.StartSpawn ();
	}


	void Init ()
	{
		canVInfect = true;
		supposedTimeToInfect = 10f;
		if (GameController.Instance.currentLevel > 0) {
			supposedTimeToInfect = 8f;
			if (GameController.Instance.currentLevel == 3) {
				supposedTimeToInfect = 6f;
			}
		}
		if (GameController.Instance.currentRound == 4) {
			if (GameController.Instance.currentLevel == 0) {
				supposedTimeToInfect = 8f;
			} else {
				supposedTimeToInfect = 6f;
			}
		}
		timeToInfect = supposedTimeToInfect;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (paused) {
			return;
		}
		if (Time.time > tickTimer + tickInterval && canTick) {
			tickEvent ();
			tickTimer = Time.time;
		}
		if (Time.time > crazyTimer + 8f) {
			tickInterval += 0.5f;
			if (tickInterval > 2f) {
				tickInterval = 2f;
			}
			ProteinManager.Instance.proteinFactor = 1;
		}
		if (Input.GetMouseButtonUp (0)) {
			EndSelection ();
			canOperate = true;
		}

		if (startSelecting) {
			focusedAnti.line.LineTo (mainCamera.ScreenToWorldPoint (Input.mousePosition));
		}

		if (antiVirus.Count < 3 && !ProteinManager.Instance.produceButton.interactable && PlayerPrefs.GetInt ("Explode") < 1) {
			Lose ();
		}
	}

	public void EndSelection ()
	{
		foreach (AntiVirus a in antiVirusOnLine) {
			a.Recover ();
		}
		antiVirusOnLine.Clear ();
		//crossResults.Clear ();
		virusToDie.Clear ();
		startSelecting = false;
		completeSelecting = false;
	}

	public void CompleteSelection ()
	{
		if (!canOperate)
			return;
		int kindFlag = 0;
		TestVirusInRange ();
		int canDestroyNum = 0;
		int incamouNum = 0;
		ProteinManager.Instance.CalcuProtein (virusToDie.ToArray ());
		foreach (Virus v in virusToDie) {
			if (v.canBeDestroyed) {
				canDestroyNum++;
				if (v is NifeVirus && kindFlag < 3) {
					kindFlag = 3;
				}
				if (v is CamouflageVirus) {
					if (kindFlag < 2)
						kindFlag = 2;
					if ((v as CamouflageVirus).inCamou)
						incamouNum++;
				}
				if (v is BombVirus || v is LittleVirus) {
					if (kindFlag < 1)
						kindFlag = 1;
				}
			}
			v.Die ();
		}
		if (canDestroyNum > 0) {
			myaudio.PlayOneShot (round);
			foreach (AntiVirus anti in antiVirusOnLine) {
				anti.Die ();
			}
			switch (kindFlag) {
			case 0:
				explodeEffect.SetTrigger ("Yellow");
				break;
			case 1:
				explodeEffect.SetTrigger ("Red");
				break;
			case 2:
				explodeEffect.SetTrigger ("Blue");
				break;
			case 3:
				explodeEffect.SetTrigger ("Purple");
				break;
			}
			if (antiVirus.Count < 1) {
				ObjectManager.Instance.Duplicate (1);
			}
			if (antiVirus.Count < 2) {
				ObjectManager.Instance.Duplicate (1);
			}
			if (canDestroyNum > incamouNum)
				ObjectManager.Instance.Duplicate ((int)((canDestroyNum - incamouNum) / 1.5f) + 2 + antiVirusOnLine.Count / 4 - GameController.Instance.currentLevel / 2);
			else
				ObjectManager.Instance.Duplicate (2);
			tickInterval -= 0.2f;
			if (tickInterval < 1.2f)
				tickInterval = 1.2f;
			ProteinManager.Instance.proteinFactor += 0.1f;
			if (canDestroyNum > 5) {
				tickInterval -= 0.4f;
				if (tickInterval < 1f)
					tickInterval = 1f;
				ProteinManager.Instance.proteinFactor = 2f;
			}
			crazyTimer = Time.time;
			if (canDestroyNum > 5)
				StartCoroutine (DelayAudio (unbelieveable));
			else if (canDestroyNum > 4)
				StartCoroutine (DelayAudio (amazing));
			else if (canDestroyNum > 2)
				StartCoroutine (DelayAudio (awesome));
			else if (canDestroyNum > 1)
				StartCoroutine (DelayAudio (nice));
			else
				StartCoroutine (DelayAudio (great));
		}
		CheckGameOver ();
		EndSelection ();
	}

	IEnumerator DelayAudio (AudioClip ac)
	{
		yield return new WaitForSeconds (0.6f);
		myaudio.PlayOneShot (ac);
	}


	void TestVirusInRange ()
	{
		Vector2[] path = new Vector2[antiVirusOnLine.Count];
		for (int i = 0; i < antiVirusOnLine.Count; i++) {
			path [i] = (Vector2)antiVirusOnLine [i].transform.position;
		}
		checkInRangeCollider.SetPath (0, path);
		foreach (Virus v in virus) {
			/*
			v.shouldRemain = false;
			Vector3 firstResult = Vector3.zero;
			for (int i = 0; i < antiVirusOnLine.Count; i++) {
				Vector3 crossResult = Vector3.Cross (antiVirusOnLine [(i + 1) % antiVirusOnLine.Count].transform.position - antiVirusOnLine [i].transform.position,
					                      v.transform.position - antiVirusOnLine [i].transform.position);
				if (i == 0) {
					firstResult = crossResult;
				}
				if (crossResult.z * firstResult.z < 0) {
					v.shouldRemain = true;
				}
			}
			if (!v.shouldRemain) {
				virusToDie.Add (v);
			}*/
			if (checkInRangeCollider.OverlapPoint ((Vector2)v.transform.position)) {
				virusToDie.Add (v);
			}
		}
	}

	public void Pause ()
	{
		Time.timeScale = 0;
		paused = true;
		menuCanvas.gameObject.SetActive (true);
	}

	public void Resume ()
	{
		menuCanvas.GetComponent <Animator> ().SetTrigger ("Resume");
	}

	public void ReStart ()
	{
		Time.timeScale = 1;
		SceneManager.LoadScene ("main");
	}

	IEnumerator Infect ()
	{
		while (true) {
			yield return new WaitUntil (() => Time.time > infectTimer + timeToInfect /*&& antiCanInfect.Count > 0*/);
			if (canVInfect && antiVirus.Count > 0 && virus.Count > 0 && !virus.All (v => v is InfectedAnti)) {
				int p = Random.Range (0, antiVirus.Count - 1);
				antiVirus [p].warningAnimator.SetTrigger ("Warning");
				//ObjectManager.Instance.AntiTransToVirus (antiCanInfect [Random.Range (0, antiCanInfect.Count - 1)]);
				infectTimer = Time.time;
				timeToInfect = supposedTimeToInfect;
				//CheckGameOver ();
				if (virus.Count > 4) {
					infectTimer -= 1.5f;
					if (virus.Count > 9) {
						infectTimer -= 1.5f;
						if (virus.Count > 14) {
							infectTimer -= 1.5f;
						}
					}
				}
			}
		}
	}

	void CalcuACenter ()
	{
		float centerX = 0;
		float centerY = 0;
		foreach (AntiVirus a in antiVirus) {
			centerX += a.transform.position.x;
			centerY += a.transform.position.y;
		}
		centerX /= antiVirus.Count;
		centerY /= antiVirus.Count;
		centerOfAnti = new Vector2 (centerX, centerY);
	}

	public void CheckGameOver ()
	{
		if (antiVirus.Count < 1 && ProteinManager.Instance.protein < 100) {
			Lose ();
			return;
		}
		if (virus.Count < 1) {
			noVirusInScene = true;
			if (ObjectManager.Instance.spawnVEnd) {
				Win ();
			}
		}
	}

	void Win ()
	{
		if (endedOnce) {
			return;
		}
		antiVirus.ForEach (a => a.warningAnimator.SetTrigger ("Cancle"));
		if (GameController.Instance.currentLevel == GameController.Instance.unlockLevel) {
			if (GameController.Instance.unlockRound < 4) {
				if (GameController.Instance.currentRound == GameController.Instance.unlockRound)
					PlayerPrefs.SetInt ("UnLockRound", ++GameController.Instance.unlockRound);
			} else {
				if (GameController.Instance.currentRound == GameController.Instance.unlockRound) {
					PlayerPrefs.SetInt ("UnLockLevel", ++GameController.Instance.unlockLevel);
					GameController.Instance.unlockRound = 0;
					PlayerPrefs.SetInt ("UnLockRound", 0);
					
				}
			}
		}
		winCanvas.GetComponentInChildren<GoldShow> (true).preGold = PlayerPrefs.GetInt ("Gold");
		BackgroundController.Instance.backgrounds [GameController.Instance.currentLevel].GetComponent <MyAudio> ().StopMusic ();
		PlayerPrefs.SetInt ("Gold", PlayerPrefs.GetInt ("Gold") + 20 + ProteinManager.Instance.protein / 10);
		StartCoroutine (DelayWin ());
		endedOnce = true;
	}

	IEnumerator DelayWin ()
	{
		yield return new WaitForSeconds (1);
		winCanvas.gameObject.SetActive (true);
		winCanvas.GetComponentInChildren<MyAudio> ().Play ();
		if (GameController.Instance.currentLevel == 3 && GameController.Instance.currentRound == 4) {
			winCanvas.transform.FindChild ("Next").GetComponent<Button> ().interactable = false;
		}
		yield return new WaitForSeconds (1);
		winCanvas.GetComponent <ShowAds> ().Show ();
	}

	IEnumerator DelayLose ()
	{
		yield return new WaitForSeconds (1);
		loseCanvas.gameObject.SetActive (true);
	}

	void Lose ()
	{
		//loseCanvas.GetComponentInChildren<GoldShow> (true).preGold = PlayerPrefs.GetInt ("Gold");
		//PlayerPrefs.SetInt ("Gold", PlayerPrefs.GetInt ("Gold") + 20 + ProteinManager.Instance.protein / 10);
		BackgroundController.Instance.backgrounds [GameController.Instance.currentLevel].GetComponent <MyAudio> ().StopMusic ();
		StartCoroutine (DelayLose ());
	}

	public void RetuenRoundSelect ()
	{
		Time.timeScale = 1;
		SceneManager.LoadScene ("Level" + (GameController.Instance.currentLevel + 1));
	}

	public void NextLevel ()
	{
		if (GameController.Instance.currentRound < 4) {
			GameController.Instance.currentRound++;
			SceneManager.LoadScene ("Level" + (GameController.Instance.currentLevel + 1));
		} else {
			GameController.Instance.currentLevel++;
			GameController.Instance.currentRound = 0;
			SceneManager.LoadScene ("LevelsSelect");
		}
	}
}
